# asbfedib

A separate background for each desktop in bspwm

This is a shell script that allows to show a different background image for each desktop of bspwm.

It is implemented with a help of `bspc subscribe` that logs each desktop event, and a simple script which continuously reads the output of `bspc subscribe`, and calls `feh` to set the corresponding wallpaper whenever a desktop is focused.

## Usage

Put the `asbfedib` script somewhere on your system, give it execute permissions with `chmod u+x asbfedib` and add the following line to your `bspwmrc`:

```
bspc subscribe desktop | asbfedib &
```

By default the script assigns files `1.jpg`, `2.jpg`, etc. located in `~/wallpapers/` directory to each desktop consecutively.